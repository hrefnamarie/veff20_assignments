# Assignments, T-213-VEFF Web Programming I, Spring 2020

## Compulsory assignments

Assignments that count towards the final grade are located in the compulsory folder. Solutions to these assignments will be published after the grading has been completed.

## Optional assignments

There are six optional assignments (last year's lab assignments). These are ungraded. We provide the solutions and the grading instructions used last year. Furthermore, we will discuss the assignments in the weekly lab sessions.
If you are new to web development, we strongly encourage you to work on these assignments first, before attempting the larger compulsory assignments.

## Forking and plagiarism

If you fork this repository and push any work on the compulsory assignments, please make sure to mark your fork private. Otherwise, the repository is public and other students can see your solutions, which then constitutes plagiarism.
